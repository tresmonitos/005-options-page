<?php

/**
 * Created by PhpStorm.
 * User: yivi
 * Date: 18/12/14
 * Time: 22:23
 */
class OJ_Options_Page {

	public $page_id;
	public $sections;
	public $fields;
	public $theme_id;
	public $parent_page;
	public $page_title;
	public $page_menu;
	public $capability;
	public $settings_key;

	function __construct( $array ) {

		$defaults = array(
			'page_id'     => 'my_options',
			'page_title'  => 'Your options',
			'page_menu'   => "Your options menu",
			'parent_page' => 'tools.php',
			'capability'  => 'manage-options',
			'sections'    => array(),
			'fields'      => array(),
			'theme_id'    => ''
		);

		$settings = wp_parse_args( $array, $defaults );

		$this->page_id     = $settings['page_id'];
		$this->page_title  = $settings['page_title'];
		$this->page_menu   = $settings['page_menu'];
		$this->parent_page = $settings['parent_page'];
		$this->capability  = $settings['capability'];
		$this->sections    = $settings['sections'];
		$this->fields      = $settings['fields'];
		$this->theme_id    = $settings['theme_id'];

		if ( $this->theme_id !== '' ) {

			$this->settings_key = 'theme_mods_' . $this->theme_id;
		} else {
			$this->settings_key = $this->page_id;
		}

		add_action( 'admin_menu', array( &$this, 'add_page' ) );
		add_action( 'admin_menu', array( &$this, 'settings_init' ) );

		add_action( 'admin_init', array( &$this, 'register_settings' ) );

	}

	public function register_settings() {
		register_setting( $this->page_id, $this->settings_key );

	}

	public function settings_init() {
		foreach ( $this->sections as $section ) {
			add_settings_section(
				$section['id'],
				$section['title'],
				function () use ( $section ) {
					echo $section['subheader'];
				},
				$this->page_id
			);

			foreach ( $section['fields'] as $field ) {
				if ( ! isset( $field['options'] ) ) {
					$field['options'] = array();
				}

				add_settings_field(
					$field['id'],
					$field['title'],
					array( &$this, 'render_field' ),
					$this->page_id,
					$section['id'],
					$field
				);


			}
		}
	}

	public function add_page() {

		$admin_page = add_submenu_page(
			$this->parent_page,
			$this->page_title,
			$this->page_menu,
			$this->capability,
			$this->page_id,
			array( &$this, 'display_page' ) );

		// add_action( 'admin_print_scripts-' . $admin_page, array( &$this, 'scripts' ) );
		// add_action( 'admin_print_styles-' . $admin_page, array( &$this, 'styles' ) );

	}

	public function display_page() {

		echo '<div class="wrap">';
		echo '<div class="icon32" id="icon-options-general"></div>';
		echo '<h2>' . $this->page_title . '</h2>';

		if ( isset( $_GET['settings-updated'] ) && $_GET['settings-updated'] == TRUE ) {
			echo '<div class="updated fade"><p>' . __( 'Theme options updated.', '005-options-page' ) . '</p></div>';
		}

		echo '<form action="options.php" method="post">';

		settings_fields( $this->page_id );
		do_settings_sections( $this->page_id );
		submit_button();
		echo "</form>";

	}

	public function render_field( $args ) {

		$options = get_option( $this->settings_key );

		if ( ! isset( $options[ $args['id'] ] ) && $args['type'] != 'checkbox' ) {
			$options[ $args['id'] ] = '';
		} elseif ( ! isset( $options[ $args['id'] ] ) ) {
			$options[ $args['id'] ] = 0;
		}

		$extra_class = '';
		if ( isset( $args['class'] ) && $args['class'] != '' ) {
			$extra_class = "class='{$args['class']}'";
		}

		switch ( $args['type'] ) {
			case 'checkbox':

				echo sprintf( '<input class="checkbox %1$s" type="checkbox" id="%2$s" name="%3$s[%2$s]" value="1" %4$s />',
					$extra_class,
					$args['id'],
					$this->settings_key,
					checked( $options[ $args['id'] ], 1, FALSE )
				);;
				if ( isset( $args['description'] ) ) {
					echo sprintf( '<label for="%1$s">%2$s</label>', $args['id'], $args['description'] );
				}

				break;

			case 'select':
				echo '<select class="select' . $extra_class . '" name="' . $this->settings_key . '[' . $args['id'] . ']">';

				foreach ( $args['choices'] as $value => $label ) {
					echo '<option value="' . esc_attr( $value ) . '"' . selected( $options[ $args['id'] ], $value, FALSE ) . '>' . $label . '</option>';
				}

				echo '</select>';

				if ( isset( $args['description'] ) ) {
					echo sprintf( '<label for="%1$s">%2$s</label>', $args['id'], $args['description'] );
				}

				break;

			case 'radio':
				$i = 0;
				foreach ( $args['choices'] as $value => $label ) {
					echo '<input class="radio' . $extra_class . '" type="radio" name="' . $this->settings_key . '[' . $args['id'] . ']" id="' . $args['id'] . $i . '" value="' . esc_attr( $value ) . '" ' . checked( $options[ $args['id'] ], $value, FALSE ) . '>';
					echo '<label for="' . $args['id'] . $i . '">' . $label . '</label>';
					if ( $i < count( $options ) - 1 ) {
						echo '<br />';
					}
					$i ++;
				}

				if ( isset( $args['description'] ) ) {
					echo sprintf( '<label for="%1$s">%2$s</label>', $args['id'], $args['description'] );
				}

				break;

			case 'textarea':
				echo '<textarea class="' . $extra_class . '" id="' . $args['id'] . '" name="' . $this->settings_key . '[' . $args['id'] . ']"  rows="5" cols="30">';
				echo format_for_editor( $options[ $args['id'] ] );
				echo '</textarea>';

				if ( isset( $args['description'] ) ) {
					echo sprintf( '<br><label for="%1$s">%2$s</label>', $args['id'], $args['description'] );
				}

				break;

			case 'password':
				echo '<input class="regular-text' . $extra_class . '" type="password" id="' . $args['id'] . '" name="' . $this->settings_key . '[' . $args['id'] . ']" value="' . esc_attr( $options[ $args['id'] ] ) . '" />';

				if ( isset( $args['description'] ) ) {
					echo sprintf( '<br><label for="%1$s">%2$s</label>', $args['id'], $args['description'] );
				}
				break;

			case 'text':
			default:
				echo '<input class="regular-text' . $extra_class . '" type="text" id="' . $args['id'] . '" name="' . $this->settings_key . '[' . $args['id'] . ']" value="' . esc_attr( $options[ $args['id'] ] ) . '" />';

				if ( isset( $args['description'] ) ) {
					echo sprintf( '<br><label for="%1$s">%2$s</label>', $args['id'], $args['description'] );
				}
				break;

		}

	}

}