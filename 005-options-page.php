<?php
/*
 * Plugin Name: 005 Ojiva Options Page Generator
 * Plugin URI: https://www.ojiva.es/
 * Description: Generate Those Options Pages.
 * Version: 0.4.1
 * Author: yivi
 * Contributors: agucho
 * Author URI: https://www.ojiva.es

 * Text Domain: 005-options-page
 * Domain Path: /lang/
 *
 * @package WordPress
 * @author yivi
 * @since 0.0.2
*/

/***************************
 * constants
 ***************************/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Load plugin class files
require_once( 'inc/OJ_Options_Page.php' );

